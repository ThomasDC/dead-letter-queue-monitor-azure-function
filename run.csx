#r "Newtonsoft.Json"
#r "Microsoft.ServiceBus"

using System;
using Microsoft.ServiceBus;

public static async Task Run(TimerInfo timer, TraceWriter log)
{
	var slackHook = "https://hooks.slack.com/services/XXXXXXXXXXXXXXXXXXXXXXXXXXXX";
	var slackChannel = "XXXXXXX";
	var servicebusConnectionString = "Endpoint=sb://XXXXXXXX.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";

	var manager = Microsoft.ServiceBus.NamespaceManager.CreateFromConnectionString(servicebusConnectionString);
	
	var client = new HttpClient();

	foreach (var queue in manager.GetQueues())
	{
		var queueName = queue.Path;

		log.Info($"Checking {queueName} for dead letter messages");

		var count = queue.MessageCountDetails.DeadLetterMessageCount;

		if (count > 0)
		{
			log.Info($"There are {count} messages in the dead letter queue of {queueName}. Sending a Slack notification...");

			// https://api.slack.com/incoming-webhooks
			var uri = new Uri(slackHook);
			var payload = Newtonsoft.Json.JsonConvert.SerializeObject(new
			{
				channel = slackChannel,
				username = "dead letter queue monitor",
				icon_emoji = ":thunder_cloud_and_rain:",
				text = $"Oh noes! The following Azure Service Bus queue contains *{count}* dead letter messages: *{queueName}*\nPlease investigate!"
			});
			
			var response = await client.PostAsync(uri, new StringContent(payload, System.Text.Encoding.UTF8, "application/json"));
			if (response.IsSuccessStatusCode)
			{
				log.Info("Message successfully posted to Slack");
			}
			else
			{
				log.Info($"Message NOT successfully posted to Slack: {response.StatusCode}");
			}
		}
	}
}
